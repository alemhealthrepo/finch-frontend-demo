import React, { useState } from 'react';
import { useFinchConnect } from 'react-finch-connect';
import './App.css';

const App = () => {
  // eslint-disable-next-line
  const [code, setCode] = useState(null);
  const axios = require('axios');

  const onSuccess = ({ code }) => {

    console.log(code)
    return axios.get(`http://localhost:5000/exchange?code=${code}`)
    .then(_ => axios.get(`http://localhost:5000/organization`)
     .then(res => {
       console.log(res.data)
     }));
  }

  const onError = ({ errorMessage }) => console.error(errorMessage);
  const onClose = () => console.log('User exited Finch Connect');

  const { open } = useFinchConnect({
    clientId: '43d7d72f-fda9-4600-bb7f-ea52527c64cc',
    // payrollProvider: '<payroll-provider-id>',
    products: ['company', 'directory', 'individual', 'employment'],
    mode: 'employer',
    onSuccess,
    onError,
    onClose,
  });

  return (
    <div className="App">
      <header className="App-header">
        <p>Code: {code}</p>
        <button type="button" onClick={() => open()}>
          Open Finch Connect
        </button>
      </header>
    </div>
  );
};
export default App;
